<?php
/** 
 ******************************************************************************
 * @desc        Post. A Wordpress component to access post_type = post
 * @package     Lib\Component\WP
 * @name        Post    
 * @copyright   (c) 2013
 ******************************************************************************/ 

namespace Voodoo\Component\WP;

use Closure;

class Post extends Wordpress
{
    public function find(Closure $fn = null)
    {
        $this->wherePostType(["post"])
            ->wherePostStatus(["publish"]);
        
        $this->select($this->tableName("posts").".*")
            ->select($this->tableName("term_relationships").".*")
            ->select($this->tableName("term_taxonomy").".*")
            ->select($this->tableName("terms").".*")
            ->join($this->tableName("term_relationships"),"{$this->tableName("posts")}.ID = {$this->tableName("term_relationships")}.object_id","","INNER")
            ->join($this->tableName("term_taxonomy"),"{$this->tableName("term_relationships")}.term_taxonomy_id = {$this->tableName("term_taxonomy")}.term_taxonomy_id","","INNER")
            ->join($this->tableName("terms"),"{$this->tableName("term_taxonomy")}.term_id = {$this->tableName("terms")}.term_id","","INNER")
            ->groupBy("ID");
        return parent::find($fn);
    }

}

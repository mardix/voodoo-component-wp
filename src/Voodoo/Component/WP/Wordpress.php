<?php
/** 
 ******************************************************************************
 * @desc        An interface to access Wordpress to be used as a CMS
 * @package     Lib\Component\WP
 * @name        Wordpress    
 * @copyright   (c) 2013
 ******************************************************************************/ 

namespace Voodoo\Component\WP;

use Voodoo;

class Wordpress extends Voodoo\Core\Model
{
    /**
     * TableName
     * 
     * @var string 
     */
    protected $tableName = "wp_posts";

    /**
     * PrimaryKey
     * 
     * @var string 
     */
    protected $primaryKeyName = "ID";
    
     /**
     * ForeignKey
     * 
     * @var string 
     */   
    protected $foreignKeyName = "%s_id";
    
     /**
     * DB Alias
     * The DB alias name in DB.ini to connect to
     * 
     * @var string 
     */   
    protected $dbAlias = "WP";    
    
    
    protected $tablePrefix = "wp_";
    
    
    private $wpContent = "";
//------------------------------------------------------------------------------
    
 
    /**
     * Return the post title
     * @return type 
     */
    public function getTitle(){
        return $this->post_title;
    }    
     

    
    /**
     * Return the first image in the post
     * @return type 
     */
    public function getContentFirstImage(){
        return $this->getFirstImage();
    }
    
    
    public function getPostName(){
        return $this->post_name;
    }

    public function getPostDate()
    {
        return $this->post_date;
    }
    
    /**
     * 
     * @return Array
     */
    public function getViewModel()
    {
        return [
            "title" => $this->getTitle(),
            "post_name" => $this->getPostName(),
            "content" => $this->getContent(),
            "first_image" => $this->getContentFirstImage(),
            "date" => $this->getPostDate(),            
        ];
    }
/*******************************************************************************/
    
    
    /**
     * Get the post 
     * @param type $postName 
     */
    public function findByPostName($postName){
        return (new static)->where([
                            "post_name" => $postName,
                            "post_status" => "publish"
                        ])
                        ->findOne();
    }   

    /**
     * Return the table name by add the prefix in it
     * @param string $table
     * @return string
     */
    protected function tableName($table)
    {
        return $this->tablePrefix.$table;
    }
/*******************************************************************************/

    /**
     * Return the formatted content
     * @return type 
     */
    public function getContent()
    {
        return
            utf8_encode(
                $this->shortcode_unautop(
                        $this->wpautop(
                                $this->convert_chars($this->post_content)
                        )
                    )
            );
    }
    
    /**
    * Create the post description
    * @param type $limit
    * @return type 
    */
    public function getExcerpt($limit=500)
    {
        return Voodoo\Core\Helpers::excerpt($this->post_content, $limit);
    }


    /**
    * Return the post first image 
    */
    public function getFirstImage()
    {
        preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $this->post_content,$matches);

        return ($matches[1][0]) ? : "";            
    }
/*******************************************************************************/

    
     /**
      * Set where in the post
      * @param type $key
      * @param type $value
      * @return Lib\Component\WP\Wordpress
      */
     public function wherePost($key,$value){
        $this->where($this->tableName("posts").".{$key}", $value);
        return $this;
     }  
     
     
    /**
     * Query by post type
     * @param Array $type
     * @return type 
     */
    public function wherePostType(Array $type){
        return $this->wherePost("post_type", $type);
    }     
 
    
    /**
     * Query by post type
     * @param Array $type
     * @return type 
     */
    public function wherePostStatus(Array $status){
        return $this->wherePost("post_status", $status);
    }    
    
    /**
     * Where tags
     * @param Array $tags
     * @return type 
     */
    public function wherePostTag(Array $tags){
        return $this->wherePostTaxonomy("post_tag", $tags);        
    }
    
    
    /**
     * Where category
     * @param Array cats
     * @return type 
     */
    public function wherePostCategory(Array $cats){
        return $this->wherePostTaxonomy("category", $cats);
    }

     /**
      * To creare a where in the taxonomy
      */
     public function wherePostTaxonomy($taxonomy, Array $slug){
         $this->where($this->tableName("term_taxonomy").".taxonomy", $taxonomy)
              ->where($this->tableName("terms").".slug", $slug);
         return $this;
     }        
    
/*******************************************************************************/
    
 
        
# FROM WORDPRESS wp-includes/formatting.php
        
         /**
         * Replaces double line-breaks with paragraph elements.
         *
         * A group of regex replaces used to identify text formatted with newlines and
         * replace double line-breaks with HTML paragraph tags. The remaining
         * line-breaks after conversion become <<br />> tags, unless $br is set to '0'
         * or 'false'.
         *
         * @since 0.71
         *
         * @param string $pee The text which has to be formatted.
         * @param int|bool $br Optional. If set, this will convert all remaining line-breaks after paragraphing. Default true.
         * @return string Text which has been converted into correct paragraph tags.
         */
        protected function wpautop($pee, $br = 1) 
        {

                if ( trim($pee) === '' )
                        return '';
                $pee = $pee . "\n"; // just to make things a little easier, pad the end
                $pee = preg_replace('|<br />\s*<br />|', "\n\n", $pee);
                // Space things out a little
                $allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|select|option|form|map|area|blockquote|address|math|style|input|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
                $pee = preg_replace('!(<' . $allblocks . '[^>]*>)!', "\n$1", $pee);
                $pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);
                $pee = str_replace(array("\r\n", "\r"), "\n", $pee); // cross-platform newlines
                if ( strpos($pee, '<object') !== false ) {
                        $pee = preg_replace('|\s*<param([^>]*)>\s*|', "<param$1>", $pee); // no pee inside object/embed
                        $pee = preg_replace('|\s*</embed>\s*|', '</embed>', $pee);
                }
                $pee = preg_replace("/\n\n+/", "\n\n", $pee); // take care of duplicates
                // make paragraphs, including one at the end
                $pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);
                $pee = '';
                foreach ( $pees as $tinkle )
                        $pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
                $pee = preg_replace('|<p>\s*</p>|', '', $pee); // under certain strange conditions it could create a P of entirely whitespace
                $pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);
                $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee); // don't pee all over a tag
                $pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee); // problem with nested lists
                $pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
                $pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);
                $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);
                $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);
                if ($br) {
                        $pee = preg_replace_callback('/<(script|style).*?<\/\\1>/s', create_function('$matches', 'return str_replace("\n", "<WPPreserveNewline />", $matches[0]);'), $pee);
                        $pee = preg_replace('|(?<!<br />)\s*\n|', "<br />\n", $pee); // optionally make line breaks
                        $pee = str_replace('<WPPreserveNewline />', "\n", $pee);
                }
                $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);
                $pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
                if (strpos($pee, '<pre') !== false)
                        $pee = preg_replace_callback('!(<pre[^>]*>)(.*?)</pre>!is', array($this,'clean_pre'), $pee );
                $pee = preg_replace( "|\n</p>$|", '</p>', $pee );

                return $pee;
        }

        /**
         * Don't auto-p wrap shortcodes that stand alone
         *
         * Ensures that shortcodes are not wrapped in <<p>>...<</p>>.
         *
         * @since 2.9.0
         *
         * @param string $pee The content.
         * @return string The filtered content.
         */
        protected function shortcode_unautop($pee) 
        {
                global $shortcode_tags;

                if ( !empty($shortcode_tags) && is_array($shortcode_tags) ) {
                        $tagnames = array_keys($shortcode_tags);
                        $tagregexp = join( '|', array_map('preg_quote', $tagnames) );
                        $pee = preg_replace('/<p>\\s*?(\\[(' . $tagregexp . ')\\b.*?\\/?\\](?:.+?\\[\\/\\2\\])?)\\s*<\\/p>/s', '$1', $pee);
                }

                return $pee;
        }   
    
        
    /**
     * Converts a number of characters from a string.
     *
     * Metadata tags <<title>> and <<category>> are removed, <<br>> and <<hr>> are
     * converted into correct XHTML and Unicode characters are converted to the
     * valid range.
     *
     * @since 0.71
     *
     * @param string $content String of characters to be converted.
     * @param string $deprecated Not used.
     * @return string Converted string.
     */
    protected function convert_chars($content) 
    {

            // Translation of invalid Unicode references range to valid range
            $wp_htmltranswinuni = array(
            '&#128;' => '&#8364;', // the Euro sign
            '&#129;' => '',
            '&#130;' => '&#8218;', // these are Windows CP1252 specific characters
            '&#131;' => '&#402;',  // they would look weird on non-Windows browsers
            '&#132;' => '&#8222;',
            '&#133;' => '&#8230;',
            '&#134;' => '&#8224;',
            '&#135;' => '&#8225;',
            '&#136;' => '&#710;',
            '&#137;' => '&#8240;',
            '&#138;' => '&#352;',
            '&#139;' => '&#8249;',
            '&#140;' => '&#338;',
            '&#141;' => '',
            '&#142;' => '&#382;',
            '&#143;' => '',
            '&#144;' => '',
            '&#145;' => '&#8216;',
            '&#146;' => '&#8217;',
            '&#147;' => '&#8220;',
            '&#148;' => '&#8221;',
            '&#149;' => '&#8226;',
            '&#150;' => '&#8211;',
            '&#151;' => '&#8212;',
            '&#152;' => '&#732;',
            '&#153;' => '&#8482;',
            '&#154;' => '&#353;',
            '&#155;' => '&#8250;',
            '&#156;' => '&#339;',
            '&#157;' => '',
            '&#158;' => '',
            '&#159;' => '&#376;'
            );

            // Remove metadata tags
            $content = preg_replace('/<title>(.+?)<\/title>/','',$content);
            $content = preg_replace('/<category>(.+?)<\/category>/','',$content);

            // Converts lone & characters into &#38; (a.k.a. &amp;)
            $content = preg_replace('/&([^#])(?![a-z1-4]{1,8};)/i', '&#038;$1', $content);

            // Fix Word pasting
            $content = strtr($content, $wp_htmltranswinuni);

            // Just a little XHTML help
            $content = str_replace('<br>', '<br />', $content);
            $content = str_replace('<hr>', '<hr />', $content);

            return $content;
    }        

    
     /**
     * Accepts matches array from preg_replace_callback in wpautop() or a string.
     *
     * Ensures that the contents of a <<pre>>...<</pre>> HTML block are not
     * converted into paragraphs or line-breaks.
     *
     * @since 1.2.0
     *
     * @param array|string $matches The array or string
     * @return string The pre block without paragraph/line-break conversion.
     */
    public function clean_pre($matches) 
    {
            if ( is_array($matches) ){
                    $text = $matches[1] . $matches[2] . "</pre>";
            }else{
                    $text = $matches;
            }
            
            $text = str_replace('<br />', '', $text);
            $text = str_replace('<p>', "\n", $text);
            $text = str_replace('</p>', '', $text);

            return $text;
    }   
    
    

        
}

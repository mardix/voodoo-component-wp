<?php

/**
 * Config
 * Include this class in /wordpress/wp-config.php
 * And replace the constant with define("DB")
 * ie: Voodoo\Component\WP\Config::getDBName();
 * 
 * @name Config
 * @author Mardix
 * @since   Aug 4, 2013
 */

namespace Voodoo\Component\WP;

use Voodoo;

class Config {
    private static $dbConfig = null;
    
    
    private static function DBConfig($key)
    {
        if (! self::$dbConfig) {
            self::$dbConfig = Voodoo\Core\Config::DB();
        }
        return self::$dbConfig->get("WP.{$key}");
    }
    
    
    public static function getDBName()
    {
        return self::DBConfig("dbname");
    }
    
    public static function getUser()
    {
        return self::DBConfig("user");
    }    

    public static function getPassword()
    {
        return self::DBConfig("password");
    }
    
    public static function getHost()
    {
        return self::DBConfig("host");
    }    
}

<?php
/** 
 ******************************************************************************
 * @desc        Page. A Wordpress component to access post_type = page
 * @package     Lib\Component\WP
 * @name        Page    
 * @copyright   (c) 2013
 ******************************************************************************/ 

namespace Voodoo\Component\WP;

use Closure;

class Page extends Wordpress
{
 
    public function find(Closure $fn = null)
    {
        $this->wherePostType(["page"])
            ->wherePostStatus(["publish"]);
        return parent::find($fn);
    }

    /**
     * To create a page menu tree based on the STRING post_name or INT post_parent
     * 
     * @param string $parentId
     * @return Array
     */
    public function createPageTreeList($parentId = 0){
        $branches = [];

        if (!is_numeric($parentId) && is_string($parentId)) {
            $parentPost = $this->findByPostName($parentId);
            $parentId = ($parentPost) ? $parentPost->getPK() : null;
        } 
        
        $pages = $this->reset()
                ->select("ID, post_title, post_parent, post_name")
                ->where("post_status", "publish")
                ->where("post_parent", $parentId)
                ->orderBy("menu_order ASC");
        
        if($pages){
            foreach ($pages as $page) {
                $subTrees = $this->createPageTreeList($page->getPK());
                $branches[] = [
                        "title" => $page->getTitle(),
                        "page_name" => $page->getPostName(),
                        "id" => $page->getPK(),
                        "children" => $subTrees,
                        "has_children" => count($subTrees) ? true : false
                    ];
            }
        }
       return $branches;
    }

    
    /**
     * Return the previous page associated to this page
     * Use for pages that is hierachiel structured
     * @return 
     */
    public function getPrevious()
    {
        if (! $this->post_parent) {
           return false; 
        } else {
            $page = (new self)
                    ->where([
                        "menu_order < ?" => $this->menu_order,
                        "post_parent = ?" => $this->post_parent
                    ])
                    ->orderBy("menu_order", "DESC")
                    ->findOne();
            
            if (! $page) {
                $page = (new self)->where("ID", $this->post_parent)->findOne();
            }
            return $page;
        }
    }
    
    
    public function getNext()
    {         
       $page = (new self)
                    ->where("post_parent", $this->getPK())
                    ->orderBy("menu_order", "ASC")
                    ->findOne();
       if(! $page) {
            $page = (new self)
                         ->where("post_parent", $this->post_parent)
                         ->where("menu_order > ?", $this->menu_order)
                         ->orderBy("menu_order", "ASC")
                         ->findOne(); 
            /**
            if(! $page) {
                 echo"<h1 style='color:blue'>2</h1>";
            $page = (new self)
                         ->where("post_parent", $this->post_parent)
                         ->where("menu_order > ?", $this->menu_order)
                         ->orderBy("menu_order", "DESC")
                         ->findOne();                 
            }
             * 
             */
       }
       return $page;
    }
}


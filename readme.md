
#Voodoo\Component\WP

A Wordpress component to access WP post, pages.

Copy this code below to /wordpress/wp-config.php
>>>>

// Include the Voodoo Lib WP component so it can use different env (dev|production)
include_once dirname(__DIR__)."/App/bootstrap.php";

/** The name of the database for WordPress */
define('DB_NAME', Voodoo\Component\WP\Config::getDBName());

/** MySQL database username */
define('DB_USER', Voodoo\Component\WP\Config::getUser());

/** MySQL database password */
define('DB_PASSWORD', Voodoo\Component\WP\Config::getPassword());

/** MySQL hostname */
define('DB_HOST', Voodoo\Component\WP\Config::getHost());

>>>>